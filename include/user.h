#ifndef _USER_H
#define _USER_H

struct User {
	int id;
	char name[50];
	char password[50];
}

struct User find_user_by_name(char *);

#endif
