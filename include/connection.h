#ifndef _CONNECTION_H
#define _CONNECTION_H

#include "user.h"

struct Connection {
	int id;
	struct User user;
};


#define MAX_CONNECTIONS 100
struct ConnectionArray {
	struct Connection connections[MAX_CONNECTIONS];
	int n;
};


/*
 * Finds a connection in the connection array by the name of a user structure 
 * that is passed to it.
 *
 * In: 
 * - connection_array: Linked list of connections.
 * - user: Pointer to a user structure.
 *
 * Return:
 * - On success returns a pointer to a connection that has a user matching the 
 *   one provided. 
 * - On failure return NULL.
 */
struct Connection *find_connection_by_name(
		struct ConnectionArray *connection_array, struct User *user);

/*
 * Finds a connection in the connection array by connection ID.
 *
 * In:
 * - connection array: Array of connections.
 * - id: Connection ID.
 *
 * Return:
 * - On success returns a pointer to a connection that has a connection id
 *   matching the id provided.
 * - On failure returns NULL.
 */
struct Connection *find_connection_by_id(
		struct ConnectionArray *connection_array, int id);

/*
 * Adds a connection to the connection array.
 *
 * In:
 * - user: User to add to the list.
 *
 * Inout:
 * - connection array: Array of connections
 *
 * Return:
 * - On success returns 0.
 * - On failure returns -1.
 */
int create_connection(struct ConnectionArray *connection_array, 
		struct User user);

#endif
