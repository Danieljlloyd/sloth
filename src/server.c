/* 
 * Author: Daniel Lloyd <Daniellloyd7@gmail.com>
 *
 * The server application reads requests from the client and associates them
 * with a connection which represents a user thread of the application. It then
 * translates the queries into operations on entities (see README) and performs
 * changes to the database accordingly.
 */

#include <stdio.h>
#include "connection.h"
#include "password.h"
#include "query.h"
#include "user.h"

int main(void)
{
	/* Create a socket */

	/* Set the socket parameters */

	/* Bind the socket to a port */

	/* Listen on the socket */

	while (1) {
		/* Accept a connection */

		/* Read from the socket */

		/* If packet starts with "LOGIN", then handle as a login 
		 * request */

			/* Split into username and password */

			/* Generate hash for the password */
			hash = generate_password_hash(password);

			/* Get the user information from the database */
			user = find_user_by_name(username);

			/* Compare with the databases password for that user,
			 * if they are the same, then reply with a connection
			 * id */

				/* Look for connection id in the existing 
				 * connections */
				connection = find_connection_by_name(
					&connections, &user);

				/* If not found, then create a new 
				 * connection */
				if (connection == NULL) {
					create_connection(&connections, &user);
				}

		/* Otherwise, this should be an existing request */

			/* Split into connection number and query */

			/* Find connection */
			connection = find_connection_by_id(&connections, id);

			/* If no connection found, return an error. */ 
			if (connection == NULL) {
				
			}

			/* Split query into arguments */
			tokenise_query(nargs, kwargs, query);
			
			/* Perform actions based on the current mode
			 * of the connection */
			switch(mode) {
				case TOP_LEVEL:
					top_level_actions(nargs, kwargs);
					break;
				case EDIT:
					edit_actions(nargs, kwargs);
					break;
				case default:
					fprintf(stderr, "This mode does not "
						"exist...\n");
					exit(-1);
			}
	}
}
