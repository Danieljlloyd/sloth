/*
 * Author: Daniel Lloyd <DanielLloyd7@gmail.com>
 *
 * On startup, a connection is obtained based on the username and password 
 * in the config file. During operation, the client application reads from 
 * standard input and sends each line as a packet to the server with the 
 * connection ID attached. 
 */

#include <stdio.h>

int main(int argc, char **argv)
{
	/* Read the config file */

	/* Try to connect to the server */

	/* If no connection was obtained, exit */

	while (1) {
		/* Read a line from stdin */

		/* Construct the query packet */

		/* Send the query */

		/* Receive the response */

		/* Print the response to stdout */
	}
}
