# Sloth

Sloth is a non-agile, multi user, internet kanban board with a command line 
interface. It aims to follow the UNIX philosophy of doing just one thing and 
doing it well.

## Design

The design consists of two applications
1. A server-side application that interfaces with a relational database 
2. An interactive shell interface that connects to the server application and 
	performs operations on a collection of Kanban boards.

### Modal, Multi-User

The application is allowed to be multi-user by assigning an enum variable
representing the current mode of a user to each connection in the connections
list. The interaction is then based on the mode via a switch statement.
This is a very neat and lightweight way of handling the situation of multiple
users and allows for the application to maintain single threaded function. 
However, it assumes that there is not multiple people using the same user at 
the same time.


### Operations

| Operation | Definition                 | Args         |
|-----------|----------------------------|--------------|
| add       | Add an entity              |              |
| edit      | Edit an entity             | id           |
| describe  | Desribe an entity          | id           |
| remove    | Remove an entity           | id           |
| move      | Move a ticket from one     | id, location |
|           | bucket to another          |              |
| list      | List entities accessible   |              |
|           | to the current user        |              |
| show      | Show an ascii table of the |              |
|           | user's personal Kanban     |              |

### Entities

| Name   | Description                                            |
|--------|--------------------------------------------------------|
| user   | A user that can interact with the boards of a group    |
| ticket | A task that is held on a board                         |
| board  | A kanban board of a groups tasks                       |
| group  | An association between collections of users and boards |

Entities are given context by the user who is logged in. There are no
administrators.

 * A user may add/edit/describe/remove users within their own groups.
 * Any user can create a new group.
 * A user can create and edit any ticket in a group that they belong to.
 * A user can only move a ticket that is assigned to them.